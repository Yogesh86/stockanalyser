var http = new require('http');
var fs = new require('fs');
var wss = new require('ws').Server({ port: 8081 });
var KiteTicker =new require("kiteconnect").KiteTicker;
const sqlite3 = new require('sqlite3').verbose();

var wsSession={};
var Actions={};
var StockHolding = {};
var OptionHolding = {};
var PostOptionAddSellCallStack = [];
//var SampleData='[{"tradable":true,"mode":"full","instrument_token":128209924,"last_price":1430.8,"last_quantity":49,"average_price":1439.7,"volume":42461,"buy_quantity":0,"sell_quantity":0,"ohlc":{"open":1449,"high":1450.9,"low":1427.75,"close":1430.8},"change":0,"last_trade_time":"2018-07-30T10:27:31.000Z","timestamp":"2018-07-30T10:30:00.000Z","oi":0,"oi_day_high":0,"oi_day_low":0,"depth":{"buy":[{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0}],"sell":[{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0},{"quantity":0,"price":0,"orders":0}]}}]';

//WS Serve:Start
wss.on('connection', (function(ws) {
  //console.log(ws.readyState);
  ws.on('open', ()=>{
    //ticker.connect();
    console.log("WS Open");
  });
  ws.on('message', (message) => {
  console.log('received:'+message);
  var msg=JSON.parse(message);
  if(msg.action=="add"){
    for(var i=0;i<msg.items.length;i++)
    {
      wsSession[msg.items[i]]=ws;
      StockHolding[msg.items[i]]={};
      //Actions.SQLGetAll(Actions.ALLOPTIONSSQL,[msg.items[i]],MultiOptionAdd);
    }
    ticker.subscribe(msg.items);
    
    switch(msg.mode)
    {
      case "ltp":
      ticker.setMode(ticker.modeLTP, msg.items);
      break;
      default:
      ticker.setMode(ticker.modeFull, msg.items);
      break;
    }
  }
  else if(msg.action=="remove")
  {
    for(var i=0;i<msg.items.length;i++)
      delete wsSession[msg.items[i]];

    ticker.unsubscribe(msg.items);
  }
  });

  ws.on('close', () => {
  console.log('Connection ended...');
  console.log(ws.readyState);
  });

  
  }));
//WS Serve:End

//Sqlite DB start
let db = new sqlite3.Database('./StockAnalyserDB.db',sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the database.');
});
//Sqlite DB End

//KiteAPI:Start
var ticker = new KiteTicker({
   		api_key: "kitefront",
   		access_token: "z8HmXARhINdeNUNSzvEtBhjQQQbG6quo"
    });
   // set autoreconnect with 10 maximum reconnections and 5 second interval
   ticker.autoReconnect(true, 10, 5);
   ticker.connect();
   ticker.on("ticks", onTicks);
   ticker.on("connect", function(){
     console.log("Connection Established")
   });
   
   ticker.on("noreconnect", function() {
   	console.log("noreconnect");
   });
   
   ticker.on("reconnect", function(reconnect_count, reconnect_interval) {
   	console.log("Reconnecting: attempt - ", reconnect_count, " interval - ", reconnect_interval);
   });
   
   function onTicks(ticks) {
     console.log(JSON.stringify(ticks));
     for(var i=0;i<ticks.length;i++)
     {
      var key=ticks[i].instrument_token;
       var data=key+","+ticks[i].last_price+","+ticks[i].mode;       
       if(ticks[i].mode=="full"){
          data+=","+ticks[i].timestamp+","+ticks[i].depth.buy[0].price+","+ticks[i].depth.sell[0].price;
          /*if(OptionHolding[key]==undefined)
            OptionHolding[key] = {};
          OptionHolding[key].buy=ticks[i].depth.buy[0].price;
          OptionHolding[key].sell=ticks[i].depth.sell[0].price;
          if(PostOptionAddSellCallStack.indexOf(key)>-1)
          {
            PostOptionAddSellCallStack.splice(PostOptionAddSellCallStack.indexOf(key),1);
            sellOption(key);
          }
          data+=","+OptionHolding[key].asset+","+OptionHolding[key].strike+","+OptionHolding[key].type+","+OptionHolding[key].sold+","+OptionHolding[key].bought;
          key=OptionHolding[key].asset;*/
        }
        /*else{
          StockHolding[key].ltp=ticks[i].last_price;
          if(StockHolding[key].CE == null || StockHolding[key].PE == null || ticks[i].last_price < StockHolding[key].CE.strike || ticks[i].last_price > StockHolding[key].PE.strike)
          {
            Actions.SQLGet(Actions.NEXTSTRIKESQL,[key,'PE',StockHolding[key].ltp],OptionAdd,"postaction");
            Actions.SQLGet(Actions.PREVSTRIKESQL,[key,'CE',StockHolding[key].ltp],OptionAdd);
          }
        }*/        
       if(wsSession[key] != undefined && wsSession[key].readyState==1)
        wsSession[key].send(data);

       //console.log(data);
     }
   }
   
    function subscribe(items) {
    //var items = [408065,20615682,20616194,20616706,20616450,20616962,20617474];
    ticker.subscribe(items);
    ticker.setMode(ticker.modeFull, items);
    console.log(items);
    }
    //KiteAPI:End
    function OptionAdd(err,row){
    //console.log(row);
    StockHolding[row.AstToken][row.Type]={strike:row.Strike,token:row.Token};
    if(OptionHolding[row.Token]==null){
      OptionHolding[row.Token] ={asset:row.AstToken,strike:row.Strike,type:row.Type};
      PostOptionAddSellCallStack.push(row.Token);
      subscribe([row.Token]);
    }
    else{
      sellOption(row.Token);
    }
    }
    function MultiOptionAdd(err,rows){
      var opts=[];
      rows.forEach((row) => {
        opts.push(row.Token);
      });
      subscribe(opts);
      //console.log(opts);
    }
    function sellOption(item){
      OptionHolding[item].sold = OptionHolding[item].buy;
    }
    function buyOption(item){
      OptionHolding[item].bought = OptionHolding[item].sell;
    }
//Server:Start
http.createServer(function (req, res) {
  //console.log(req);
  var url=req.url=="/"?"home.htm":req.url.substring(1);
  switch(req.method)
  {
    case "POST":
      var payload = '';
      req.on('data',function(d){
        payload+=d;
      })
      req.on('end', function () {
        //console.log(payload);
        switch(url)
        {
          case "Action":
            console.log(Actions[payload]);
            if(typeof(Actions[payload])=="function")
              Actions[payload].call(this,res);
          break;
          default:
          res.end();
          break;
        }        
      });      
    break;
    case "GET":
    fs.readFile(url, function(err, data) {
      //console.log(url.split(".")[1]);
      switch(url.split(".")[1])
      {
        case "ico":
          res.writeHead(200, {'Content-Type': 'image/x-icon'} );
        break;
        case "js":
          res.writeHead(200, {'Content-Type': 'text/javascript'} );
        break;
        default:
          res.writeHead(200, {'Content-Type': 'text/html','Set-Cookie': '__cfduid=d2bda501c90ed5be2cb0171ee44e793841531480157'});        
          break;        
      }
      if(err)
      {
        console.log(err);
        res.writeHead(404);
        res.write(JSON.stringify(err));
      }
      else
        res.write(data);
      res.end();
    });
    break;
    default:
      res.end();
    break;
  }
}).listen(8080);
//Server End

/* Actions.UpdateInstrumentsFromFile=function(){
  console.log("in function UpdateInstrumentsFromFile");
  var XLSX = require('xlsx');
  var workBook = XLSX.readFile('Input.xlsx');
  var xlData = XLSX.utils.sheet_to_json(workBook.Sheets[workBook.SheetNames[0]]);
  console.log(xlData.length);
  return "Success";
} */

Actions.PREVSTRIKESQL = "select *,? 'cb_param' from stockoptions where asttoken = ? and type = ? and strike < ? order by strike desc";
Actions.NEXTSTRIKESQL = "select *,? 'cb_param' from stockoptions where asttoken = ? and type = ? and strike > ? order by strike"
Actions.ALLOPTIONSSQL = "select *,? 'cb_param' from stockoptions where asttoken = ?";

Actions.SQLRun = function(sql,param){
  db.run(sql,param, function(err) {
    if (err) {
      return console.error(err.message);
    }
    console.log(`Row(s) affected ${this.changes}`);
  });
}
Actions.SQLGet = function(sql,param,cb,cbParm){
  param.unshift(cbParm);
  db.get(sql,param,cb);
  /*(err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(`${row}`);
  }*/
}
Actions.SQLGetAll = function(sql,param,cb,cbParm){
  param.unshift(cbParm);
  db.all(sql,param,cb);
}

Actions.FetchStocks = function(res){
  db.all("select distinct symbol,asttoken from stockoptions;",[],function(err,rows){
    if (err)
      res.write(err.message);
    else
      res.write(JSON.stringify(rows));
    res.end();
  });
}
Actions.FetchFutures = function(res){
  db.all("select distinct symbol,token from futures order by symbol;",[],function(err,rows){
    if (err)
      res.write(err.message);
    else
      res.write(JSON.stringify(rows));
    res.end();
  });
}

